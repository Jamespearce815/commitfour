package org.domco.connectfour.console.presentation;

import org.apache.commons.lang3.StringUtils;
import org.domco.connectfour.console.presentation.GameParameters.GameParametersBuilder;
import org.domco.connectfour.game.domain.GameStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Scanner;

/**
 * Game class to start the game
 */
public class Game {

    /**
     * Log.
     */
    private static final Logger logger = LoggerFactory.getLogger(Game.class);

    //Messages to print out to user when requesting game parameters.
    private static final String GAME_WIDTH_MESSAGE = "Please choose the game width. (For standard rules set equal to 7)";
    private static final String GAME_HEIGHT_MESSAGE = "Please choose the game height. (For standard rules set equal to 6)";
    private static final String CONSECUTIVE_PIECES_FOR_WIN_MESSAGE = "Please choose the number of consecutive pieces required for a win. (For standard rules set equal to 4)";
    private static final String PLAYER_TWO_HANDICAP_MESSAGE = "Please choose player two's handicap. (For standard rules set equal to 0)";

    private static int gameWidth;


    /**
     * Create an instance of the game to play.
     */
    public static void main(String[] args) throws IOException, InterruptedException {

        //To clear the bash window for start of the game.
        System.out.print("\033[H\033[2J");
        //To clear the command prompt for start of game
        new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();

        PlayUserMove playUserMove = chooseDefaultOrCustomGame();
        printGameGridInConsole(playUserMove.createConsoleGridOfGameState(playUserMove.getCurrentGameState()));
        boolean gameInProgress = true;

        do {

            int chosenColumn = takeUserColumnSelection();
            String updatedGameGrid = playUserMove.playMove(chosenColumn);
            printGameGridInConsole(updatedGameGrid);

            if (!playUserMove.isGameStateSuccessfullyUpdated()) {
                System.out.println("Please choose another move. The previous move was not valid.");
            }

            GameStatus gameStatus = playUserMove.getGameStatus();

            if (!gameStatus.equals(GameStatus.UNDETERMINED)) {

                System.out.println("GAME OVER.");

                if (gameStatus.equals(GameStatus.RED_WIN)) {
                    System.out.println("Congratulations RED Player! You Win!");
                } else if (gameStatus.equals(GameStatus.YELLOW_WIN)) {
                    System.out.println("Congratulations YELLOW Player! You Win!");
                } else {
                    System.out.println("It's a tie!");
                }

                gameInProgress = false;
            }

        } while (gameInProgress);
    }

    /**
     * @return the playUserMove object after the user has decided to play with the default settings or input their own.
     */
    private static PlayUserMove chooseDefaultOrCustomGame() {
        System.out.println("Would you like to play with the standard game parameters? (Y/n)");
        Scanner scanner = new Scanner(System.in);
        while (!scanner.hasNext("[YyNn]")) {
            System.out.println("You must enter \"Y\" or \"n\" to proceed.");
            scanner.next();
        }
        String gameParameterChar = scanner.next();

        PlayUserMove playUserMove;

        if (StringUtils.equals(gameParameterChar, "Y") || StringUtils.equals(gameParameterChar, "y")) {

            gameWidth = 7;
            GameParameters defaultGameParameters = new GameParametersBuilder()
                    .withGameWidth(gameWidth)
                    .withGameHeight(6)
                    .withConsecutivePiecesForWin(4)
                    .withPlayerTwoHandicap(0)
                    .build();
            playUserMove = new PlayUserMove(defaultGameParameters);

        } else {

            playUserMove = new PlayUserMove(takeUserGameParameterChoice());

        }
        return playUserMove;
    }


    /**
     * @return The GameParameters object which the user has chosen.
     */
    private static GameParameters takeUserGameParameterChoice() {

        gameWidth = takeUserParameterChoice(GAME_WIDTH_MESSAGE);
        final int gameHeight = takeUserParameterChoice(GAME_HEIGHT_MESSAGE);
        final int consecutivePiecesForWin = takeUserParameterChoice(CONSECUTIVE_PIECES_FOR_WIN_MESSAGE);
        final int playerTwoHandicap = takeUserParameterChoice(PLAYER_TWO_HANDICAP_MESSAGE);

        return new GameParametersBuilder()
                .withGameWidth(gameWidth)
                .withGameHeight(gameHeight)
                .withConsecutivePiecesForWin(consecutivePiecesForWin)
                .withPlayerTwoHandicap(playerTwoHandicap)
                .build();

    }

    /**
     * @param message The message to display to the user if they do not make a valid choice for the requested parameter
     * @return verified int value of the parameter
     */
    private static int takeUserParameterChoice(String message) {

        System.out.println(message);
        Scanner scanner = new Scanner(System.in);
        while (!scanner.hasNextInt()) {
            System.out.println(message);
            scanner.nextLine();
        }

        int parameter = scanner.nextInt();
        int minValue;

        if (message.equals(PLAYER_TWO_HANDICAP_MESSAGE)) {
            minValue = 0;
        } else {
            minValue = 1;
        }

        if (parameter < minValue) {
            parameter = takeUserParameterChoice(message);
        }

        return parameter;

    }

    /**
     * @return verified int value of the column user wants to play in.
     */
    private static int takeUserColumnSelection() {

        System.out.println("Drop a disk in columns 0-" + (gameWidth - 1));
        Scanner scanColumn = new Scanner(System.in);
        while (!scanColumn.hasNextInt()) {
            System.out.println("Drop a disk in columns 0-" + (gameWidth - 1));
            scanColumn.nextLine();
        }
        int userMove = scanColumn.nextInt();
        if (userMove < 0 || userMove > gameWidth - 1) {
            userMove = takeUserColumnSelection();
        }

        return userMove;
    }

    /**
     * Prints the game grid in the console.
     *
     * @param gameGrid Specified game grid to print.
     */
    private static void printGameGridInConsole(String gameGrid) throws IOException, InterruptedException {

        logger.debug("Displaying the chosen game grid.");

        //To clear the bash window for start of the game.
        System.out.print("\033[H\033[2J");
        //To clear the command prompt of the previous move.
        new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();

        System.out.println(gameGrid);

        logger.debug("Game grid displayed in the console.");

    }

}

FROM openjdk:8-jdk-alpine

RUN mkdir connectfour
ADD https://bitbucket.org/DMist/connectfour/get/develop.gzip connectfour.tar.gz
RUN tar -xzf connectfour.tar.gz --strip-components=1 -C connectfour
WORKDIR /connectfour
RUN ./gradlew build

EXPOSE 8080

ENTRYPOINT ./gradlew bootRun

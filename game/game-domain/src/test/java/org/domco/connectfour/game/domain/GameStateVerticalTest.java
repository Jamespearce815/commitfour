package org.domco.connectfour.game.domain;

import org.domco.connectfour.game.domain.GamePiece.GamePieceBuilder;
import org.domco.connectfour.game.domain.GameState.GameStateBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Game state vertical win test
 */
public class GameStateVerticalTest {

    /**
     * Set of game pieces
     */
    private Set<GamePiece> gamePieceSet;

    /**
     * Width of a standard game.
     */
    private static final int STANDARD_GAME_WIDTH = 7;

    /**
     * Height of a standard game.
     */
    private static final int STANDARD_GAME_HEIGHT = 6;

    /**
     * Number of consecutive pieces that are the same colour required for the game to be won.
     */
    private static final int CONSECUTIVE_PIECES_FOR_WIN = 4;

    /**
     * Player two handicap.
     * When HANDICAP = 0 both players get an equal amount of turns per go.
     */
    private static final int HANDICAP = 0;

    @Before
    public void setup() {
        gamePieceSet = new HashSet<>();
    }

    /**
     * Checks that the vertical win is correctly detected.
     * Game state under test:
     * 5 |  |  |  |  |  |  |  |
     * 4 |  |Y?|  |  |  |  |  |
     * 3 |  |Y |  |  |  |  |  |
     * 2 |  |Y |  |  |  |  |  |
     * 1 |Y |Y |R |R |  |  |  |
     * 0 |R |R |Y |R |R |R |Y |
     * ...0  1  2  3  4  5  6
     */
    @Test
    public void checkForVerticalWinOne() {

        // Setup
        // 1)Create a game state that is one move away from a vertical win

        List<GamePiece> preExistingPieces = Arrays.asList(

                new GamePieceBuilder().withxPosition(0).withyPosition(0).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(0).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(0).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(2).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(2).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(2).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(0).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(3).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(0).withPieceColour(PieceColour.RED).build()

        );

        // 2)Create the game state.
        gamePieceSet.addAll(preExistingPieces);
        GameState startingGameState = new GameStateBuilder()
                .withGameWidth(STANDARD_GAME_WIDTH)
                .withGameHeight(STANDARD_GAME_HEIGHT)
                .withColourPlayed(PieceColour.RED)
                .withConsecutivePiecesForWin(CONSECUTIVE_PIECES_FOR_WIN)
                .withPlayerTwoHandicap(HANDICAP)
                .withGamePieceSet(gamePieceSet)
                .build();

        // Test

        // 3) Check GameStatus is undetermined
        GameStatus gameStatus = startingGameState.getGameStatus();
        Assert.assertEquals("There shouldn't be a vertical win yet", GameStatus.UNDETERMINED, gameStatus);

        // 4) Play the move which will end the game
        GamePiece finalPiece = new GamePiece.GamePieceBuilder()
                .withxPosition(1)
                .withyPosition(4)
                .withPieceColour(PieceColour.YELLOW).build();
        gamePieceSet.add(finalPiece);

        GameState endGameState = new GameStateBuilder()
                .withGameWidth(STANDARD_GAME_WIDTH)
                .withGameHeight(STANDARD_GAME_HEIGHT)
                .withColourPlayed(PieceColour.YELLOW)
                .withConsecutivePiecesForWin(CONSECUTIVE_PIECES_FOR_WIN)
                .withPlayerTwoHandicap(HANDICAP)
                .withGamePieceSet(gamePieceSet)
                .build();

        gameStatus = endGameState.getGameStatus();
        Assert.assertEquals("The game should now be over due to a vertical win", GameStatus.YELLOW_WIN, gameStatus);

    }

    /**
     * Checks that the vertical win is correctly detected.
     * Game state under test:
     * 5 |  |  |  |  |R?|  |  |
     * 4 |  |R |  |  |R |  |  |
     * 3 |R |Y |  |  |R |Y |  |
     * 2 |Y |Y |  |  |R |Y |  |
     * 1 |Y |Y |R |R |Y |Y |  |
     * 0 |R |R |Y |R |R |R |Y |
     * ...0  1  2  3  4  5  6
     */
    @Test
    public void checkForVerticalWinTwo() {

        // Setup
        // 1)Create a game state that is one move away from a vertical win

        List<GamePiece> preExistingPieces = Arrays.asList(

                new GamePieceBuilder().withxPosition(0).withyPosition(0).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(0).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(0).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(2).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(2).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(2).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(0).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(3).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(0).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(2).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(4).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(2).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(4).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(3).withPieceColour(PieceColour.YELLOW).build()

        );

        // 2)Create the game state.
        gamePieceSet.addAll(preExistingPieces);
        GameState startingGameState = new GameStateBuilder()
                .withGameWidth(STANDARD_GAME_WIDTH)
                .withGameHeight(STANDARD_GAME_HEIGHT)
                .withColourPlayed(PieceColour.YELLOW)
                .withConsecutivePiecesForWin(CONSECUTIVE_PIECES_FOR_WIN)
                .withPlayerTwoHandicap(HANDICAP)
                .withGamePieceSet(gamePieceSet)
                .build();

        // Test

        // 3) Check GameStatus is undetermined
        GameStatus gameStatus = startingGameState.getGameStatus();
        Assert.assertEquals("There shouldn't be a vertical win yet", GameStatus.UNDETERMINED, gameStatus);

        // 4) Play the move which will end the game
        GamePiece finalPiece = new GamePieceBuilder()
                .withxPosition(4)
                .withyPosition(5)
                .withPieceColour(PieceColour.RED).build();
        gamePieceSet.add(finalPiece);

        GameState endGameState = new GameStateBuilder()
                .withGameWidth(STANDARD_GAME_WIDTH)
                .withGameHeight(STANDARD_GAME_HEIGHT)
                .withColourPlayed(PieceColour.RED)
                .withConsecutivePiecesForWin(CONSECUTIVE_PIECES_FOR_WIN)
                .withPlayerTwoHandicap(HANDICAP)
                .withGamePieceSet(gamePieceSet)
                .build();

        gameStatus = endGameState.getGameStatus();
        Assert.assertEquals("The game should now be over due to a vertical win", GameStatus.RED_WIN, gameStatus);

    }

}

package org.domco.connectfour.game.domain;

import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.filters.*;
import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.rule.impl.GetterMustExistRule;
import com.openpojo.validation.rule.impl.SetterMustExistRule;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;
import com.openpojo.validation.utils.ValidationHelper;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Constructor;
import java.util.List;

/**
 * Model test.
 */
public class ModelTest {

    /** Log. */
    private static final Logger LOG = LoggerFactory.getLogger(ModelTest.class);

    /** The package to test. */
    private static final String POJO_PACKAGE = ModelTest.class.getPackage()
        .getName();

    /** Class filter. */
    private final FilterChain classFilter = new FilterChain(new FilterPackageInfo(), new FilterNonConcrete(),
            new FilterEnum(), new FilterClassName("^((?!Test$).)*$"),
            new FilterClassName("^((?!ClazzMapperDecorator$).)*$"));

    /** Class filter. */
    private final FilterChain equalsClassFilter = new FilterChain(new FilterPackageInfo(), new FilterNonConcrete(),
            new FilterEnum(), new FilterClassName("^((?!Test$).)*$"), new FilterClassName("!ClazzMapperDecorator"));

    /**
     * Check POJO structure and behaviour.
     */
    //@Test
    public void checkPojoStructureAndBehaviour() {

        final Validator validator = ValidatorBuilder.create()
            .with(new GetterMustExistRule())
            .with(new SetterMustExistRule())
            .with(new SetterTester())
            .with(new GetterTester())
            .build();

        validator.validate(POJO_PACKAGE, classFilter);
    }

    /**
     * Check equals contract.
     */
    //@Test
    public void checkEqualsContract() {

        final List<PojoClass> pojoClasses = PojoClassFactory.getPojoClassesRecursively(POJO_PACKAGE, equalsClassFilter);

        for (final PojoClass pojoClass : pojoClasses) {
            LOG.debug("Validating equals of class {}", pojoClass.getName());
            EqualsVerifier.forClass(pojoClass.getClazz())
                .suppress(Warning.NONFINAL_FIELDS)
                .verify();
        }
    }

    /**
     * Check to string.
     *
     * @throws Throwable
     *             Throwable.
     */
    //@Test
    public void checkToString() throws Throwable {

        final List<PojoClass> pojoClasses = PojoClassFactory.getPojoClassesRecursively(POJO_PACKAGE, classFilter);

        for (final PojoClass pojoClass : pojoClasses) {
            for (final Constructor<?> constructor : pojoClass.getClazz()
                .getDeclaredConstructors()) {
                final Object object;

                if (0 == constructor.getParameterTypes().length) {
                    object = pojoClass.getClazz()
                        .newInstance();
                } else {
                    object = ValidationHelper.getBasicInstance(pojoClass);
                }

                LOG.debug("Checking class {}", pojoClass.getClazz()
                    .getName());

                // Asserts
                Assert.assertNotEquals(object.toString(), invokeSpecial(object));
            }
        }
    }

    /**
     * Invoke special.
     *
     * @param obj
     *            Object.
     *
     * @return Result of toString.
     * @throws Throwable
     *             Throwable.
     */
    private String invokeSpecial(final Object obj) throws Throwable {

        // create the lookup
        final Constructor<MethodHandles.Lookup> methodHandlesLookupConstructor = MethodHandles.Lookup.class
            .getDeclaredConstructor(Class.class);

        methodHandlesLookupConstructor.setAccessible(true);

        final MethodHandles.Lookup lookup = methodHandlesLookupConstructor.newInstance(obj.getClass());

        // create the method handle
        final MethodHandle handle = lookup.findSpecial(Object.class, "toString", MethodType.methodType(String.class),
                obj.getClass());

        return (String) handle.invokeWithArguments(obj);
    }
}

package org.domco.connectfour.game.service;

import org.domco.connectfour.game.domain.GameState;

/**
 * Interface for Game State Factory
 */
public interface GameStateFactory {

    /**
     * Creates an empty game state that is 7 wide and 6 high.
     *
     * @return Empty game state.
     */
    GameState createEmptyStandardGameState();

    /**
     * Creates a game state that is 7 wide and 6 high with pieces random inserted.
     *
     * @return Empty game state.
     */
    GameState createRandomStandardGameState();

}

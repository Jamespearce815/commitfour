# ConnectFour

[![CircleCI (develop)](https://img.shields.io/circleci/project/bitbucket/DMist/connectfour/develop.svg)](https://circleci.com/bb/DMist/connectfour)

[![Coverage Status](https://coveralls.io/repos/bitbucket/DMist/connectfour/badge.svg?branch=coverage)](https://coveralls.io/bitbucket/DMist/connectfour?branch=coverage)

This project is to demonstrate REST and automated testing principles.

A live version can be found at https://connect4.richteaman.com. The game should be run in Firefox or Google Chrome as Internet Explorer is not currently supported.

## Gradle Tasks
This project uses [Gradle](https://gradle.org/)!
You can use the wrapper instead of installing Gradle

Examples of how to run through command line using the wrapper include:

* gradlew.bat build
* gradlew.bat test
* gradlew.bat bootRun

To run using Git Bash instead of command line, replace gradlew.bat with ./gradlew for all commands.

##Getting started

* The below sections provide the necessary information to run the application and automated test packs through command line or IntelliJ.

* For a more step by step guide on getting started and an overview of the project please visit the [project wiki article](https://bitbucket.org/DMist/connectfour/wiki/Home)

## Running the Server
The server is run locally as a Sprint Boot
application. This can be done from the command line:

```
gradlew.bat build bootRun
```

Once the server is running navigate to http://localhost:8080

## API Tests

The API tests use the [Gherkin language](https://docs.cucumber.io/gherkin/reference/) and [REST assured](http://rest-assured.io/) for integration tests.

To run the tests the server needs to be running. Then, in a separate terminal instance, run:
```
gradlew.bat apiTest
```

## UI Tests

The UI tests use the [Gherkin language](https://docs.cucumber.io/gherkin/reference/) and [Frameworkium](https://frameworkium.github.io/) for ui tests.

To run the tests the server needs to be running. Then, in a separate terminal instance, run:
```
gradlew.bat uiTest
```

## Importing into IntelliJ
This project needs two idea plugins before it can be run in Intellij. Install both before importing the project:

* [The Gherkin plugin](https://plugins.jetbrains.com/plugin/7211-gherkin)
* [The Cucumber for Java plugin](https://plugins.jetbrains.com/plugin/7212-cucumber-for-java)

IntelliJ may install the wrong plugins if you choose to let it automatically find and install the necessary plugins for this project

The server and automated test packs can also be run in the IDE.

You can run the server by running the server-controller as a Java application. The main class is:

* 'org.domco.connectfour.server.controller.application.ServerRunner'.

You can run or debug the UI and API tests through the IDE using the cucumber feature files.

* The UI cucumber tests are in the 'connectFour\server\server-ui-test\src\test\resources' folder

* The API cucumber tests are in the 'connectFour\server\server-controller-integration-test\src\test\resources' folder

## Docker and Jenkins
This project has a dockerfile. While in the connectfour project directory, build the image with:
```
sudo docker build -t connectfour .
```

Run the image with:
```
sudo docker run -d -p 8080:8080 connectfour
```
The image takes a few minutes to start, but should be listening on port 8080 after the build has completed.

This project has a Jenkins file to automatically build and run the docker image on your jenkins pipeline. It can be found in the project root.

## API Documentation

A JSON Swagger spec can be found at '/v2/api-docs'. Locally this will be 'http://localhost:8080/v2/api-docs', but a live
version can also be found at 'https://connect4.richteaman.com/v2/api-docs'

A UI version can be seen using the [Swagger Demo page](http://petstore.swagger.io/?url=https://connect4.richteaman.com/v2/api-docs).

## Console Application

A simple console version can be built with:
```
./gradlew build fatJar
```

It can then be run by navigating to the console presentation output folder (connectfour/consoleUi/console-presentation/build/libs) and running Java:
```
user@computer ~/connectfour/consoleUi/console-presentation/build/libs
$ java -jar console-presentation-all.jar
```

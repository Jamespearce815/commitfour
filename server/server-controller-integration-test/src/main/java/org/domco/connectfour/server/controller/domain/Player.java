package org.domco.connectfour.server.controller.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Player object
 */
public class Player {

    private final String playerName;
    private final String playerId;

    private Player(final PlayerBuilder builder) {
        this.playerName = builder.playerName;
        this.playerId = builder.playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public String getPlayerId() {
        return playerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        return new EqualsBuilder()
                .append(playerName, player.playerName)
                .append(playerId, player.playerId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(playerName)
                .append(playerId)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("playerName", playerName)
                .append("playerId", playerId)
                .toString();
    }

    /**
     * Builder for the Player class
     */
    public static class PlayerBuilder {

        private String playerName;
        private String playerId;

        public Player build() {
            return new Player(this);
        }

        public PlayerBuilder withPlayerName(final String playerName) {
            this.playerName = playerName;
            return this;
        }

        public PlayerBuilder withPlayerId(final String playerId) {
            this.playerId = playerId;
            return this;
        }

    }

}

package org.domco.connectfour.server.controller.domain;

import java.util.Set;

/**
 * Player game state.
 */
public class PlayerGameState extends ConnectfourDTO<PlayerGameState> {

    private final String playerName;

    private final String opponentName;

    private final PlayerColour playerColour;

    private final PlayerColour opponentColour;

    private final PlayerColour turnColour;

    private final GameStatus gameStatus;

    private final String winner;

    private final int gameWidth;

    private final int gameHeight;

    private final Set<GamePiece> gamePieceSet;

    private PlayerGameState(final GameStateBuilder builder) {

        this.playerName = builder.playerName;
        this.opponentName = builder.opponentName;
        this.playerColour = builder.playerColour;
        this.opponentColour = builder.opponentColour;
        this.turnColour = builder.turnColour;
        this.gameStatus = builder.gameStatus;
        this.winner = builder.winner;
        this.gameWidth = builder.gameWidth;
        this.gameHeight = builder.gameHeight;
        this.gamePieceSet = builder.gamePieceSet;

    }


    public String getPlayerName() {
        return playerName;
    }

    public String getOpponentName() {
        return opponentName;
    }

    public PlayerColour getPlayerColour() {
        return playerColour;
    }

    public PlayerColour getOpponentColour() {
        return opponentColour;
    }

    public PlayerColour getTurnColour() {
        return turnColour;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public String getWinner() {
        return winner;
    }

    public int getGameWidth() {
        return gameWidth;
    }

    public int getGameHeight() {
        return gameHeight;
    }

    public Set<GamePiece> getGamePieceSet() {
        return gamePieceSet;
    }

    /**
     * Builder for the PlayerGameState class
     */
    public static class GameStateBuilder {

        private String playerName;

        private String opponentName;

        private PlayerColour playerColour;

        private PlayerColour opponentColour;

        private PlayerColour turnColour;

        private GameStatus gameStatus;

        private String winner;

        private int gameWidth;

        private int gameHeight;

        private Set<GamePiece> gamePieceSet;

        public PlayerGameState build() {
            return new PlayerGameState(this);
        }

        public GameStateBuilder withPlayerName(final String playerName) {
            this.playerName = playerName;
            return this;
        }

        public GameStateBuilder withOpponentName(final String opponentName) {
            this.opponentName = opponentName;
            return this;
        }

        public GameStateBuilder withPlayerColour(final PlayerColour playerColour) {
            this.playerColour = playerColour;
            return this;
        }

        public GameStateBuilder withOpponentColour(final PlayerColour opponentColour) {
            this.opponentColour = opponentColour;
            return this;
        }

        public GameStateBuilder withTurnColour(final PlayerColour turnColour) {
            this.turnColour = turnColour;
            return this;
        }

        public GameStateBuilder withGameStatus(final GameStatus gameStatus) {
            this.gameStatus = gameStatus;
            return this;
        }

        public GameStateBuilder withWinner(final String winner) {
            this.winner = winner;
            return this;
        }

        public GameStateBuilder withGameWidth(final int gameWidth) {
            this.gameWidth = gameWidth;
            return this;
        }

        public GameStateBuilder withGameHeight(final int gameHeight) {
            this.gameHeight = gameHeight;
            return this;
        }

        public GameStateBuilder withGamePieceSet(final Set<GamePiece> gamePieceSet) {
            this.gamePieceSet = gamePieceSet;
            return this;
        }

    }

}

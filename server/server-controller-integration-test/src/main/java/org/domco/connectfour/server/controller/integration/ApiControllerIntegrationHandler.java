package org.domco.connectfour.server.controller.integration;

import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.domco.connectfour.server.controller.constants.ConnectFourEndpoint;
import org.domco.connectfour.server.controller.domain.PlayerGameState;
import org.springframework.stereotype.Component;

/**
 * All integration methods for the connect4 api
 */
@Component
public class ApiControllerIntegrationHandler extends ConnectFourIntegration implements ApiControllerIntegration {

    /**
     * {@inheritDoc}
     */
    @Override
    public String playerRequestsToJoinGame(String playerName) {
        return get(String.format(ConnectFourEndpoint.JOIN_GAME, playerName))
                .log().all()
                .assertThat().statusCode(HttpStatus.SC_OK)
                .extract()
                .response()
                .asString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PlayerGameState retrievePlayersGameState(String playerId) {
        RestAssured.defaultParser = Parser.JSON;
        return get(String.format(ConnectFourEndpoint.RETRIEVE_GAME, playerId))
                .log().all()
                .extract()
                .as(PlayerGameState.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PlayerGameState playGamePiece(String playerId, int columnNum) {
        return get(String.format(ConnectFourEndpoint.PLAY_GAME_PIECE, playerId, columnNum))
                .log().all()
                .assertThat().statusCode(HttpStatus.SC_OK)
                .extract()
                .as(PlayerGameState.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ValidatableResponse deleteAllPlayers() {
        return delete(ConnectFourEndpoint.DELETE_ALL_PLAYERS)
                .log().all()
                .assertThat()
                .statusCode(HttpStatus.SC_OK);

    }
}

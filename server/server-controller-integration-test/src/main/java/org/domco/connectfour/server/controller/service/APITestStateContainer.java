package org.domco.connectfour.server.controller.service;

import org.domco.connectfour.server.controller.domain.Game;
import org.domco.connectfour.server.controller.domain.PlayerGameState;

/**
 * Interface for the state of the application throughout each test.
 */
public interface APITestStateContainer {

    /**
     * @return The Game object to contain the state of the game throughout the test.
     */
    Game getGame();

    /**
     * Sets the Game object to contain the state of the game throughout the test.
     */
    void setGame(Game game);

    /**
     * Replaces the outdated playerGameState object with an updated one that
     * reflects the state of the playerGameState at this point in the game
     *
     * @param playerGameState        the PlayerGameState object that does not reflect the new action that has been performed in the test
     * @param updatedPlayerGameState the PlayerGameState object that is up-to-date with the state of the player at this point in the game
     */
    void addUpdatedPlayerToGameObject(PlayerGameState playerGameState, PlayerGameState updatedPlayerGameState);

}

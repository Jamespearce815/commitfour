package org.domco.connectfour.server.controller.stepdefs.domainobjects;

import org.domco.connectfour.server.controller.domain.GamePiece;

import java.util.Set;

/**
 * Game state information object to hold information from cucumber data table
 */
public class GameStateInformation {

    private final String playerName;

    private final String opponentName;

    private final String playerColour;

    private final String opponentColour;

    private final String turnColour;

    private final String gameStatus;

    private final String winner;

    private final int gameWidth;

    private final int gameHeight;

    private final Set<GamePiece> gamePieceset;

    public GameStateInformation(String playerName, String opponentName, String playerColour, String opponentColour,
                                String turnColour, String gameStatus, String winner, int gameWidth,
                                int gameHeight, Set<GamePiece> gamePieceset) {
        this.playerName = playerName;
        this.opponentName = opponentName;
        this.playerColour = playerColour;
        this.opponentColour = opponentColour;
        this.turnColour = turnColour;
        this.gameStatus = gameStatus;
        this.winner = winner;
        this.gameWidth = gameWidth;
        this.gameHeight = gameHeight;
        this.gamePieceset = gamePieceset;
    }

    public String getPlayerName() {
        return playerName;
    }

    public String getOpponentName() {
        return opponentName;
    }

    public String getPlayerColour() {
        return playerColour;
    }

    public String getOpponentColour() {
        return opponentColour;
    }

    public String getTurnColour() {
        return turnColour;
    }

    public String getGameStatus() {
        return gameStatus;
    }

    public String getWinner() {
        return winner;
    }

    public int getGameWidth() {
        return gameWidth;
    }

    public int getGameHeight() {
        return gameHeight;
    }

    public Set<GamePiece> getGamePieceset() {
        return gamePieceset;
    }
}

package org.domco.connectfour.server.controller.validation;

import org.domco.connectfour.server.controller.domain.Game;
import org.domco.connectfour.server.controller.domain.Player;
import org.domco.connectfour.server.controller.service.APITestStateContainer;
import org.junit.Assert;
import org.springframework.stereotype.Component;

/**
 * Implementation of the Api Controller Validator.
 */
@Component
public class ApiControllerValidatorHandler implements ApiControllerValidator {

    private String uuidRegex = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}";

    private APITestStateContainer apiTestStateContainer;

    public ApiControllerValidatorHandler (APITestStateContainer apiTestStateContainer){
        this.apiTestStateContainer = apiTestStateContainer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void assertPlayerIdIsPresent(Player player) {
        Assert.assertTrue(
                player.getPlayerName() + " did not have a valid key after requesting to join: " + player.getPlayerId(),
                player.getPlayerId().matches(uuidRegex)
        );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void assertActualGameMatchesExpected(Game expectedGame) {
        Game actualGame = apiTestStateContainer.getGame();
        Assert.assertEquals(expectedGame, actualGame);
    }

}

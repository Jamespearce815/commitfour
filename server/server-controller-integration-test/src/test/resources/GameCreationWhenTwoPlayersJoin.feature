Feature: When two players request to join a game the game is created

  Scenario: client makes a call to the api to join a game with one player waiting. a game is created and both players
  are added.
    Given two users "yellowPlayer" and "redPlayer" have chosen to join a game
    Then a game is created for the players with the following states
      | playerName   | opponentName | playerColour | opponentColour | turnColour | gameStatus   | winner | gameWidth | gameHeight |
      | yellowPlayer | redPlayer    | yellow       | red            | red        | undetermined |        | 7         | 6          |
      | redPlayer    | yellowPlayer | red          | yellow         | red        | undetermined |        | 7         | 6          |
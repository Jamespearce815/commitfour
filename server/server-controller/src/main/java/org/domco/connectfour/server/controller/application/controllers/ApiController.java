package org.domco.connectfour.server.controller.application.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import org.domco.connectfour.game.domain.PieceColour;
import org.domco.connectfour.game.service.GameStateFactory;
import org.domco.connectfour.server.domain.Game;
import org.domco.connectfour.server.domain.GamePlayer;
import org.domco.connectfour.server.domain.PlayerMoveResult;
import org.domco.connectfour.server.model.GameStateModel;
import org.domco.connectfour.server.model.mappers.GameStateMapper;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * API Controller. All handling methods on
 * this controller are relative to the /api path
 */
@RestController
@RequestMapping("/api")
@EnableAutoConfiguration
public class ApiController {

    private GameStateFactory gameStateFactory;

    private GameStateMapper gameStateMapper;

    private final Map<String, Game> gameCache = new HashMap<>();

    private Map<String, GamePlayer> playerLookup = new HashMap<>();

    /**
     * Creates a player and queues them for a game.
     *
     * @param playerName
     *            The player's name. This is only for display purposes.
     * @return Player ID.
     */
    @ApiOperation("Returns a player ID that can be checked to see if the player is in a game. This token should be used with retrieveGame.")
    @ApiResponses({ @ApiResponse(code = 200, message = "Successfully retrieved player ID.") })
    @RequestMapping(value = "/join", method = { RequestMethod.GET, RequestMethod.POST })
    @ResponseBody
    public String joinGame(@ApiParam("The player's name. This is only for display purposes.") final String playerName) {

        final GamePlayer gamePlayer = new GamePlayer(playerName);

        // try to find waiting player

        final Optional<GamePlayer> waitingGamePlayerOptional = playerLookup.values()
                .stream()
                .filter(player -> player.getGame() == null)
                .findFirst();
        if (waitingGamePlayerOptional.isPresent()) {

            final GamePlayer waitingGamePlayer = waitingGamePlayerOptional.get();

            gamePlayer.setPieceColour(PieceColour.RED);
            waitingGamePlayer.setPieceColour(PieceColour.YELLOW);

            final List<GamePlayer> players = new ArrayList<>();
            players.add(gamePlayer);
            players.add(waitingGamePlayer);

            // create game
            final Game game = new Game(gameStateFactory.createEmptyStandardGameState(), players);

            gamePlayer.setGame(game);
            waitingGamePlayer.setGame(game);

            gameCache.put(game.getId(), game);
        }

        playerLookup.put(gamePlayer.getKey(), gamePlayer);

        return gamePlayer.getKey();
    }

    /**
     * Gets the current state of the player's game.
     *
     * @param playerId
     *            The ID of the player.
     * @return Player's game.
     */
    @ApiOperation("Gets the current state of the player's game.")
    @ApiResponses({ @ApiResponse(code = 200, message = "Successfully retrieved player's game."),
        @ApiResponse(code = 404, message = "The given player ID is not recognised."),

    })
    @RequestMapping(value = "/retrieveGame", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<GameStateModel> retrieveGame(
            @ApiParam("Player ID.") @RequestParam(value = "playerId", required = true) final String playerId) {

        final GamePlayer gamePlayer = playerLookup.get(playerId);
        if (gamePlayer == null) {
            return new ResponseEntity<GameStateModel>((GameStateModel) null, HttpStatus.NOT_FOUND);
        }

        if (gamePlayer.getGame() == null) {
            return new ResponseEntity<GameStateModel>((GameStateModel) null, HttpStatus.ACCEPTED);
        }

        final GameStateModel gameStateModel = gameStateMapper.mapToModel(gamePlayer.getGame(), playerId);
        return new ResponseEntity<GameStateModel>(gameStateModel, HttpStatus.OK);
    }

    /**
     * Plays the given move for the given player.
     *
     * @param playerId
     *            Player ID.
     * @param position
     *            Zero based column index.
     * @return Returns the updated game grid if successful, otherwise returns an
     *         error.
     */
    @ApiOperation("Player a game piece for the given player.")
    @ApiResponses({ @ApiResponse(code = 200, message = "Successfully played a piece."),
        @ApiResponse(code = 404, message = "The given player ID is not recognised."),
        @ApiResponse(code = 204, message = "The game is over."),
        @ApiResponse(code = 409, message = "The given move is not playable, either because the column is full or it is out of bounds."),
        @ApiResponse(code = 401, message = "It is not the given player's turn."), })
    @RequestMapping(value = "/playGamePiece", method = { RequestMethod.GET, RequestMethod.POST })
    @ResponseBody
    public ResponseEntity<GameStateModel> PlayGamePiece(
            @ApiParam("Player ID.") @RequestParam(value = "playerId", required = true) final String playerId,
            @ApiParam("Zero based column index.") @RequestParam(value = "position", required = true) final int position) {

        final ResponseEntity<GameStateModel> response;

        final GamePlayer gamePlayer = playerLookup.get(playerId);
        if (null != gamePlayer) {
            final PlayerMoveResult playerMoveResult = gamePlayer.playMove(position);

            final GameStateModel gameStateModel = gameStateMapper.mapToModel(gamePlayer.getGame(), playerId);

            switch (playerMoveResult) {
            case GAME_NOT_READY:
                response = new ResponseEntity<GameStateModel>(gameStateModel, HttpStatus.ACCEPTED);
                break;
            case GAME_OVER:
                response = new ResponseEntity<GameStateModel>(gameStateModel, HttpStatus.NO_CONTENT);
                break;
            case MOVE_NOT_AVAILABLE:
                response = new ResponseEntity<GameStateModel>(gameStateModel, HttpStatus.CONFLICT);
                break;
            case MOVE_OUT_OF_BOUNDS:
                response = new ResponseEntity<GameStateModel>(gameStateModel, HttpStatus.CONFLICT);
                break;
            case NOT_PLAYERS_TURN:
                response = new ResponseEntity<GameStateModel>(gameStateModel, HttpStatus.UNAUTHORIZED);
                break;
            case SUCCESS:
                response = new ResponseEntity<GameStateModel>(gameStateModel, HttpStatus.OK);
                break;
            default:
                throw new IllegalStateException("Unknown player move result " + playerMoveResult);
            }
        } else {
            response = new ResponseEntity<GameStateModel>((GameStateModel) null, HttpStatus.NOT_FOUND);
        }

        return response;
    }

    /**
     * This is am endpoint to erase all players currently playing or waiting to play.
     * TODO Remove this endpoint and erase players through JMX
     */
    @ApiOperation("Clears the map of all player's and their IDs.")
    @ApiResponses({@ApiResponse(code = 200, message = "Successfully deleted all players")})
    @RequestMapping(value = "/deleteAllPlayers", method = {RequestMethod.DELETE})
    @ResponseBody
    public void clearAllPlayers(){
        playerLookup = new HashMap<>();
    }

    public void setPlayerLookup(Map<String, GamePlayer> playerLookup) {
        this.playerLookup = playerLookup;
    }

    @Inject
    public void setGameStateFactory(final GameStateFactory gameStateFactory) {
        this.gameStateFactory = gameStateFactory;
    }

    @Inject
    public void setGameStateMapper(final GameStateMapper gameStateMapper) {
        this.gameStateMapper = gameStateMapper;
    }

}
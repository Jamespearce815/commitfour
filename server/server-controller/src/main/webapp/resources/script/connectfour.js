function populateGameGrid(destinationElementId, gameGrid) {

	var html = '<table class="game-grid">';
	
	html += "<tr>";
	for (var x = 0; x < gameGrid.gameWidth; x++) {
	    html += `<th><button class="piece-button" data-column="${x}" >Drop</button></th>`;
	}
	html += "</tr>";
	
	for (var y = 0; y < gameGrid.gameHeight; y++) {
		html += "<tr>";
		for (var x = 0; x < gameGrid.gameWidth; x++) {
			
			var transformedY = gameGrid.gameHeight - (y + 1);
			html += `<td id="square-${x}-${transformedY}" class="piece-square"></td>`;
		}
		html += "</tr>";
	}
	html += "</table>";
	
	$(`#${destinationElementId}`).html(html);
	
	for (var i = 0; i < gameGrid.gamePieceSet.length; i++) {

		var piece = gameGrid.gamePieceSet[i];
		var id = `#square-${piece.xPosition}-${piece.yPosition}`;
		
		$(id).addClass(piece.pieceColour);
	}
}

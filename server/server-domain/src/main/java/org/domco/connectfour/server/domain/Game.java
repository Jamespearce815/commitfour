package org.domco.connectfour.server.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.domco.connectfour.game.domain.GameMove;
import org.domco.connectfour.game.domain.GameState;
import org.domco.connectfour.game.domain.GameStatus;

import java.util.*;

/**
 * Game.
 */
public final class Game {

    private final String id;

    private GameState gameState;

    private final List<GamePlayer> gamePlayerList;

    private String winner = null;

    public PlayerMoveResult playMove(final int columnNumber, final GamePlayer gamePlayer) {

        PlayerMoveResult playerMoveResult;

        if (columnNumber >= 0 && columnNumber < gameState.getGameWidth()) {

            if (gameState.getNextPieceColour() == gamePlayer.getPieceColour()) {

                final Set<GameMove> gameMoves = gameState.calculateAvailableMoves();

                final Optional<GameMove> gameMoveOptional = gameMoves.stream()
                        .filter(move -> move.getPlayedPiece().getxPosition() == columnNumber
                        && move.getPlayedPiece().getPieceColour() == gamePlayer.getPieceColour())
                        .findFirst();

                if (gameMoveOptional.isPresent()) {
                    final GameState updatedGameState = gameMoveOptional.get().getEndGameState();
                    gameState = updatedGameState;

                    playerMoveResult = PlayerMoveResult.SUCCESS;

                    if(gameState.getGameStatus() == GameStatus.YELLOW_WIN
                            ||gameState.getGameStatus() == GameStatus.RED_WIN){
                        winner = gamePlayer.getName();
                    }

                    if(gameState.getGameStatus() == GameStatus.TIE){
                        winner = "It's a draw!!";
                    }

                } else {
                    playerMoveResult = PlayerMoveResult.MOVE_NOT_AVAILABLE;
                }
            } else {
                playerMoveResult = PlayerMoveResult.NOT_PLAYERS_TURN;
            }
        } else {
            playerMoveResult = PlayerMoveResult.MOVE_OUT_OF_BOUNDS;
        }

        return playerMoveResult;
    }


    public GameState getGameState() {
        return gameState;
    }

    public String getId() {
        return id;
    }

    public List<GamePlayer> getGamePlayerList() {
        return Collections.unmodifiableList(gamePlayerList);
    }

    public String getWinner() { return winner; }

    public Game(final GameState gameState, final List<GamePlayer> gamePlayerList) {
        this.id = UUID.randomUUID().toString();
        this.gameState = gameState;
        this.gamePlayerList = gamePlayerList;
    }


    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id)
                .append(gameState)
                .append(gamePlayerList)
                .toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        boolean equals = false;
        if (obj instanceof Game) {
            final Game rhs = (Game) obj;
            equals = new EqualsBuilder().append(id, rhs.id)
                    .append(gameState, rhs.gameState)
                    .append(gamePlayerList, rhs.gamePlayerList)
                    .isEquals();
        }
        return equals;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id)
                .append("gameState", gameState)
                .append("gamePlayerList", gamePlayerList)
                .toString();
    }

}

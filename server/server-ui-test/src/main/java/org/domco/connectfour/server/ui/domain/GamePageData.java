package org.domco.connectfour.server.ui.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Domain object for the corresponding Game Page object.
 */
public class GamePageData {

    final private String connectFourTitle;
    final private String waitingMessage;

    //Used to obtain the unique player id in the url
    private String playerId;

    private GamePageData(final GamePageDataBuilder builder) {
        this.connectFourTitle = builder.connectFourTitle;
        this.waitingMessage = builder.waitingMessage;
        this.playerId = builder.playerId;
    }

    public String getConnectFourTitle() {
        return connectFourTitle;
    }

    public String getWaitingMessage() {
        return waitingMessage;
    }

    public String getPlayerId() {
        return playerId;
    }

    //Not final as the uuid is tricky to assert so taken out of some test assertions
    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        GamePageData gamePageData = (GamePageData) o;

        return new EqualsBuilder()
                .append(connectFourTitle, gamePageData.connectFourTitle)
                .append(waitingMessage, gamePageData.waitingMessage)
                .append(playerId, gamePageData.playerId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(connectFourTitle)
                .append(waitingMessage)
                .append(playerId)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("connectFourTitle", connectFourTitle)
                .append("waitingMessage", waitingMessage)
                .append("playerId", playerId)
                .toString();
    }

    /**
     * Builder for the GamePageData class
     */
    public static class GamePageDataBuilder {

        private String connectFourTitle;
        private String waitingMessage;
        //Used to obtain the unique player id in the url
        private String playerId;

        public GamePageData build() {
            return new GamePageData(this);
        }

        public GamePageDataBuilder withConnectFourTitle(final String connectFourTitle) {
            this.connectFourTitle = connectFourTitle;
            return this;
        }

        public GamePageDataBuilder withWaitingMessage(final String waitingMessage) {
            this.waitingMessage = waitingMessage;
            return this;
        }

        public GamePageDataBuilder withPlayerId(final String playerId) {
            this.playerId = playerId;
            return this;
        }

    }

}

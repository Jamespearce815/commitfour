package org.domco.connectfour.server.ui.domain;

/**
 * Enum of connect 4 player colours.
 */
public enum PlayerColour {

    /**
     * Player is red.
     */
    RED,

    /**
     * Player is yellow.
     */
    YELLOW,

    /**
     * A game has not begun so colours have not been assigned to waiting players.
     */
    UNDEFINED
}

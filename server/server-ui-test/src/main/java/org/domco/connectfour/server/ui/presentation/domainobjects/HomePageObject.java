package org.domco.connectfour.server.ui.presentation.domainobjects;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.domco.connectfour.server.ui.presentation.domainobjects.components.NewGameForm;
import org.domco.connectfour.server.ui.presentation.domainobjects.components.TitleBar;

/**
 * Page Object of the Home Page
 */
public class HomePageObject extends BasePage<HomePageObject> {

    //Title bar component
    @Visible
    private TitleBar titleBar;

    //New Game Form component
    @Visible
    private NewGameForm newGameForm;

    public TitleBar getTitleBar() {
        return titleBar;
    }

    public NewGameForm getNewGameForm() {
        return newGameForm;
    }

}

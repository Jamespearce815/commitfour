package org.domco.connectfour.server.ui.presentation.domainobjects.components;

import com.frameworkium.core.ui.pages.PageFactory;
import org.domco.connectfour.server.ui.domain.GamePiece;
import org.domco.connectfour.server.ui.domain.PlayerColour;
import org.domco.connectfour.server.ui.presentation.domainobjects.GamePageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The @FindBy tag can't be used on fields in the GameGrid due to the grid being updated twice a second.
 * This results in the element being outdated by the time the test comes to interact with it. The methods in this class
 * allow for the stale element and wait for the elements to become updated before trying to interact.
 */
@FindBy(css = ".game-grid > tbody")
public class GameGrid extends HtmlElement {

    private final String rowsCssSelector = "tr:not(:first-child)";
    @FindBy(css = rowsCssSelector)
    private List<WebElement> rows;

    private final String redCellsCssSelector = "tr:not(:first-child) td.piece-square.RED";
    @FindBy(css = redCellsCssSelector)
    private List<WebElement> redCells;

    private final String yellowCellsCssSelector = "tr:not(:first-child) td.piece-square.YELLOW";
    @FindBy(css = yellowCellsCssSelector)
    private List<WebElement> yellowCells;

    private final String dropButtonHeaderCellsCssSelector = ".piece-button";
    @FindBy(css = dropButtonHeaderCellsCssSelector)
    private List<Button> dropButtonHeaderCells;

    /**
     * Fetches all rows on the game gird
     *
     * @return All rows on the game gird.
     */
    public List<WebElement> fetchRows(WebDriver driver) {

        List<WebElement> allRows = new ArrayList<>();

        new WebDriverWait(driver, 10)
                .ignoring(StaleElementReferenceException.class)
                .until((WebDriver d) -> {
                    List<WebElement> rows = new ArrayList<>(d.findElements(By.cssSelector(rowsCssSelector)));
                    allRows.addAll(rows);
                    return true;
                });
        return allRows;
    }

    /**
     * Fetches all the cells for a given row.
     *
     * @param indexOfRow Row of the cells that need to get fetched
     * @return All cells for the chosen row
     */
    public List<WebElement> fetchRowCells(WebDriver driver, int indexOfRow) {

        List<WebElement> allRowCells = new ArrayList<>();

        new WebDriverWait(driver, 10)
                .ignoring(StaleElementReferenceException.class)
                .until((WebDriver d) -> {
                    List<WebElement> rowCells = new ArrayList<>(
                            d.findElements(By.cssSelector(rowsCssSelector))
                                    .stream().skip(indexOfRow)
                                    .findFirst()
                                    .map(element -> element.findElements(By.cssSelector("td")))
                                    .get()
                    );

                    allRowCells.addAll(rowCells);
                    return true;
                });
        return allRowCells;
    }

    /**
     * Collects the ids of all cells that contain a red piece.
     * The Ids contain the position of the piece in the game grid
     * For example: id = square-0-0 is the piece in position (0,0)
     *
     * @return list of the ids of all red pieces in the grid
     */
    private List<String> fetchPositionsOfRedPieces(WebDriver driver) {

        List<String> allPositionsOfRedPieces = new ArrayList<>();

        new WebDriverWait(driver, 10)
                .ignoring(StaleElementReferenceException.class)
                .until((WebDriver d) -> {
                    List<String> positionsOfRedPieces =
                            d.findElements(By.cssSelector(redCellsCssSelector))
                                    .stream()
                                    .map(redCell -> redCell.getAttribute("id"))
                                    .collect(Collectors.toList());

                    allPositionsOfRedPieces.addAll(positionsOfRedPieces);
                    return true;
                });

        return allPositionsOfRedPieces;

    }

    /**
     * Collects the ids of all cells that contain a yellow piece.
     * The ids contain the position of the piece in the game grid
     * For example: id = square-0-0 is the piece in position (0,0)
     *
     * @return list of the ids of all yellow pieces in the grid
     */
    private List<String> fetchPositionsOfYellowPieces(WebDriver driver) {

        List<String> allPositionsOfYellowPieces = new ArrayList<>();

        new WebDriverWait(driver, 10)
                .ignoring(StaleElementReferenceException.class)
                .until((WebDriver d) -> {
                    List<String> positionsOfYellowPieces =
                            d.findElements(By.cssSelector(yellowCellsCssSelector))
                                    .stream()
                                    .map(yellowCell -> yellowCell.getAttribute("id"))
                                    .collect(Collectors.toList());

                    allPositionsOfYellowPieces.addAll(positionsOfYellowPieces);
                    return true;
                });

        return allPositionsOfYellowPieces;

    }

    /**
     * Clicks the drop button for the specified column
     *
     * @param columnNumber column number the player wants to play their piece in
     * @param driver       web driver to allow for ignoring of StaleElementRefException
     * @return updated game page with the new move that has been played.
     */
    public GamePageObject clickDropButtonHeaderCell(int columnNumber, WebDriver driver) {
        new WebDriverWait(driver, 10)
                .ignoring(StaleElementReferenceException.class)
                .until((WebDriver d) -> {
                    d.findElements(By.cssSelector(dropButtonHeaderCellsCssSelector)).get(columnNumber).click();
                    return true;
                });
        return PageFactory.newInstance(GamePageObject.class);
    }

    /**
     * After the game is over the drop buttons become disabled.
     * This method collects all drop buttons still enabled.
     * If the list if empty the game is empty and false is returned.
     *
     * @param driver web driver to allow for ignoring of StaleElementRefException
     * @return boolean for if the game is in progress
     */
    public boolean gameIsInProgress(WebDriver driver) {

        List<WebElement> enabledButtons = new ArrayList<>();

        new WebDriverWait(driver, 10)
                .ignoring(StaleElementReferenceException.class)
                .until((WebDriver d) -> {
                    List<WebElement> listOfEnabledButtons = d.findElements(By.cssSelector(dropButtonHeaderCellsCssSelector))
                            .stream()
                            .filter(WebElement::isEnabled)
                            .collect(Collectors.toList());

                    enabledButtons.addAll(listOfEnabledButtons);
                    return true;
                });
        return !enabledButtons.isEmpty();

    }

    /**
     * Builds a set of Game Pieces from the list of red and yellow pieces on the game gird
     *
     * @param driver web driver to allow for ignoring of StaleElementRefException
     * @return set of gamePieces that represents the state of the game.
     */
    public Set<GamePiece> constructGamePieceSet(WebDriver driver) {

        Set<GamePiece> gamePieces = new HashSet<>();
        List<String> redPiecePositions = fetchPositionsOfRedPieces(driver);

        List<String> yellowPiecePositions = fetchPositionsOfYellowPieces(driver);

        for (String redPiecePosition : redPiecePositions) {
            GamePiece piece = new GamePiece.GamePieceBuilder()
                    .withxPosition(Integer.parseInt(redPiecePosition.substring(7, 8)))
                    .withyPosition(Integer.parseInt(redPiecePosition.substring(9, 10)))
                    .withPieceColour(PlayerColour.RED)
                    .build();

            gamePieces.add(piece);
        }

        for (String yellowPiecePosition : yellowPiecePositions) {
            GamePiece piece = new GamePiece.GamePieceBuilder()
                    .withxPosition(Integer.parseInt(yellowPiecePosition.substring(7, 8)))
                    .withyPosition(Integer.parseInt(yellowPiecePosition.substring(9, 10)))
                    .withPieceColour(PlayerColour.YELLOW)
                    .build();

            gamePieces.add(piece);
        }


        return gamePieces;
    }

}

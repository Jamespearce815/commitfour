package org.domco.connectfour.server.ui.presentation.domainobjects.components;

import org.apache.commons.lang3.StringUtils;
import org.domco.connectfour.server.ui.constants.Constant;
import org.domco.connectfour.server.ui.domain.GameStatus;
import org.domco.connectfour.server.ui.domain.PlayerColour;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

/**
 * Component for all web elements in the Game Information section
 */
@FindBy(css = "#body > div:nth-child(2)")
public class GameInformation extends HtmlElement {

    @FindBy(id = "grid")
    private WebElement waitingMessage;

    @FindBy(id = "player_name")
    private WebElement playerName;

    @FindBy(id = "opponent_name")
    private WebElement opponentName;

    @FindBy(id = "colour")
    private WebElement colourToPlay;

    @FindBy(id = "endOfGame")
    private WebElement endOfGameInformation;

    private static final Logger logger = LoggerFactory.getLogger(GameInformation.class);


    /**
     * Grabs the waiting message text.
     * This should be empty if the game has started but populated if not.
     *
     * @return Waiting message text
     */
    public String fetchWaitingMessageText() {
        return waitingMessage.getText();
    }

    /**
     * @return PlayerName text
     */
    public String fetchPlayerNameText() {
        return playerName.getText();
    }

    /**
     * @return OpponentName text
     */
    public String fetchOpponentNameText() {
        return opponentName.getText();
    }

    /**
     * @return The colour of the player who's turn it is to move
     */
    private PlayerColour fetchColourToPlay() {

        String colour = colourToPlay.getAttribute("class");
        PlayerColour playerColour;

        if (StringUtils.contains(colour, PlayerColour.RED.toString())) {
            playerColour = PlayerColour.RED;
        } else if (StringUtils.contains(colour, PlayerColour.YELLOW.toString())) {
            playerColour = PlayerColour.YELLOW;
        } else {
            throw new IllegalStateException("Colour to play was neither "+ Constant.RED +" or " + Constant.YELLOW );
        }

        return playerColour;
    }

    /**
     * @return The colour of the player who just moved.
     */
    public PlayerColour fetchColourJustPlayed () {

        PlayerColour colourJustPlayed;

        if(fetchColourToPlay().equals(PlayerColour.RED)){
            colourJustPlayed = PlayerColour.YELLOW;
        } else if (fetchColourToPlay().equals(PlayerColour.YELLOW)) {
            colourJustPlayed = PlayerColour.RED;
        } else {
            logger.debug("Colour just played is undefined. The game has not begun yet");
            colourJustPlayed = PlayerColour.UNDEFINED;
        }

        return colourJustPlayed;
    }

    /**
     * @return The current state of the game.
     */
    public GameStatus fetchGameStatus() {

        String endOfGameText = endOfGameInformation.getText();
        GameStatus gameStatus;

        if (StringUtils.isEmpty(endOfGameText)) {

            gameStatus = GameStatus.UNDETERMINED;

        } else if (endOfGameText.contains("It's a draw!!")) {

            gameStatus = GameStatus.TIE;


        } else {

            if (endOfGameText.contains(fetchPlayerNameText())) {

                gameStatus = assignWinStatus(fetchPlayerColour());

            } else if (endOfGameText.contains(fetchOpponentNameText())) {

                gameStatus = assignWinStatus(fetchOpponentColour());

            } else {
                throw new IllegalStateException("The Game Status did not match one of the expected outcomes.");
            }

        }

        return gameStatus;

    }

    /**
     * @param playerColour colour of the player who has won the game
     * @return Game status enum for the yellow winner or red winner
     */
    private GameStatus assignWinStatus(PlayerColour playerColour) {

        GameStatus gameStatus;

        if (playerColour.equals(PlayerColour.RED)) {

            gameStatus = GameStatus.RED_WIN;

        } else if (playerColour.equals(PlayerColour.YELLOW)) {

            gameStatus = GameStatus.YELLOW_WIN;

        } else {
            throw new IllegalStateException("The PlayerColour did not match one of the expected colours.");

        }

        return gameStatus;
    }

    /**
     * @return the player colour for the given player's web element
     */
    public PlayerColour fetchPlayerColour() {

        String colour = playerName.getAttribute("class");
        PlayerColour playerColour;

        if (StringUtils.equals(colour, PlayerColour.RED.toString())) {
            playerColour = PlayerColour.RED;
        } else if (StringUtils.equals(colour, PlayerColour.YELLOW.toString())) {
            playerColour = PlayerColour.YELLOW;
        } else {
            logger.debug("The player colour has not yet been set. Another player needs to join.");
            playerColour = PlayerColour.UNDEFINED;
        }

        return playerColour;
    }

    /**
     * @return the player colour for the given player's web element
     */
    private PlayerColour fetchOpponentColour() {

        String colour = opponentName.getAttribute("class");
        PlayerColour playerColour;

        if (StringUtils.equals(colour, PlayerColour.RED.toString())) {
            playerColour = PlayerColour.RED;
        } else if (StringUtils.equals(colour, PlayerColour.YELLOW.toString())) {
            playerColour = PlayerColour.YELLOW;
        } else {
            throw new IllegalStateException("The player has no assigned colour");
        }

        return playerColour;
    }

}

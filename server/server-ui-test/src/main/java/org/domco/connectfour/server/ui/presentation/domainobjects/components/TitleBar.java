package org.domco.connectfour.server.ui.presentation.domainobjects.components;

import com.frameworkium.core.ui.annotations.Visible;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.Link;

/**
 * Component for all elements on the title bar.
 */
@FindBy(css = "#body h1")
public class TitleBar extends HtmlElement {

    @Visible
    @FindBy(linkText = "Connect Four.")
    private Link title;

    /**
     * @return The title link text
     */
    public String fetchTitleText() {
        return title.getText();
    }
}

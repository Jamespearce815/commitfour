package org.domco.connectfour.server.ui.presentation.mappers;

import com.frameworkium.core.ui.pages.PageFactory;
import org.domco.connectfour.server.ui.domain.*;
import org.domco.connectfour.server.ui.domain.GamePageData.GamePageDataBuilder;
import org.domco.connectfour.server.ui.domain.GamePlayer.GamePlayerBuilder;
import org.domco.connectfour.server.ui.domain.GameState.GameStateBuilder;
import org.domco.connectfour.server.ui.domain.UiPageData.UiPageDataBuilder;
import org.domco.connectfour.server.ui.presentation.domainobjects.GamePageObject;
import org.domco.connectfour.server.ui.presentation.domainobjects.components.GameGrid;
import org.domco.connectfour.server.ui.presentation.service.BrowserInteractionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.HashSet;
import java.util.Set;


/**
 * Mapper class for mapping from Page Object to Domain Object for the Game Page
 */
@Component
public class GamePagePresentationMapperHandler implements GamePagePresentationMapper {

    private static final Logger logger = LoggerFactory.getLogger(GamePagePresentationMapperHandler.class);

    private BrowserInteractionService browserInteractionService;

    @Inject
    public GamePagePresentationMapperHandler(BrowserInteractionService browserInteractionService) {
        this.browserInteractionService = browserInteractionService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GamePlayer mapToGamePlayer(GamePlayer gamePlayer) {

        logger.debug("Attempting to map the Game Page Object to a corresponding Game Player Object");

        GamePageObject gamePageObject = PageFactory.newInstance(GamePageObject.class);

        //Building updated game page data for this player
        GamePageData gamePageData = new GamePageDataBuilder()
                .withConnectFourTitle(gamePageObject.getTitleBar().fetchTitleText())
                .withWaitingMessage(gamePageObject.getGameInformation().fetchWaitingMessageText())
                .withPlayerId(gamePageObject.fetchPlayerId())
                .build();

        //Build uiPageData without changing homePage as it hasn't changed.
        UiPageData uiPageData = new UiPageDataBuilder()
                .withHomePageData(gamePlayer.getUiPageData().getHomePageData())
                .withGamePageData(gamePageData)
                .build();

        //Build updated game player
        GamePlayer updatedGamePlayer = new GamePlayerBuilder()
                .withPlayerName(gamePageObject.getGameInformation().fetchPlayerNameText())
                .withPlayerColour(gamePageObject.getGameInformation().fetchPlayerColour())
                .withOpponentName(gamePageObject.getGameInformation().fetchOpponentNameText())
                .withGameStatus(gamePageObject.getGameInformation().fetchGameStatus())
                .withGameState(fetchUpdatedGameState(gamePageObject))
                .withUiPageData(uiPageData)
                .build();


        logger.debug("Returning the mapped Player object");

        return updatedGamePlayer;
    }


    /**
     * Builds the Game State based on the Game grid from the Game page object
     *
     * @return GameState for the Player object
     */
    private GameState fetchUpdatedGameState(GamePageObject gamePageObject) {

        GameGrid gameGrid = gamePageObject.getGameGrid();

        //Create an empty game state for if the game has not begun yet
        GameState gameState = new GameStateBuilder()
                .withColourPlayed(PlayerColour.UNDEFINED)
                .withGamePieceSet(new HashSet<>())
                .build();

        if (gamePageObject.isVisible(gameGrid)) {

            //If no moves have been played set the colour just played as undefined. Else check who's go it isn't.
            Set<GamePiece> gamePieceSet = gameGrid.constructGamePieceSet(browserInteractionService.getDriver());
            PlayerColour colourPlayed;
            if (gamePieceSet.isEmpty()) {
                colourPlayed = PlayerColour.UNDEFINED;
            } else {
                colourPlayed = gamePageObject.getGameInformation().fetchColourJustPlayed();
            }

            gameState = new GameStateBuilder()
                    .withGameHeight(gameGrid.fetchRows(browserInteractionService.getDriver()).size())
                    .withGameWidth(gameGrid.fetchRowCells(browserInteractionService.getDriver(), 0).size())
                    .withGamePieceSet(gamePieceSet)
                    .withColourPlayed(colourPlayed)
                    .build();

        }
        return gameState;
    }

}

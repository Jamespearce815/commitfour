package org.domco.connectfour.server.ui.presentation.mappers;

import org.domco.connectfour.server.ui.domain.HomePageData;
import org.domco.connectfour.server.ui.presentation.domainobjects.HomePageObject;

/**
 * Interface for the Home Page Presentation Mapper.
 */
public interface HomePagePresentationMapper {

    /**
     * @return the domain object for the Home page
     */
    HomePageData mapToHomeDto();

}
package org.domco.connectfour.server.ui.presentation.mappers;

import com.frameworkium.core.ui.pages.PageFactory;
import org.domco.connectfour.server.ui.constants.Constant;
import org.domco.connectfour.server.ui.domain.HomePageData;
import org.domco.connectfour.server.ui.domain.HomePageData.HomePageDataBuilder;
import org.domco.connectfour.server.ui.presentation.domainobjects.HomePageObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Mapper class for mapping Home Page Object to corresponding Home Page Domain Object
 */
@Component
public class HomePagePresentationMapperHandler implements HomePagePresentationMapper{

    private static final Logger logger = LoggerFactory.getLogger(HomePagePresentationMapperHandler.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public HomePageData mapToHomeDto() {

        logger.debug("Attempting to map the Home Page Object into a corresponding HomePageData");

        return new HomePageDataBuilder()
                .withConnectFourTitle(PageFactory.newInstance(HomePageObject.class, Constant.BASE_URI)
                        .getTitleBar()
                        .fetchTitleText())
                .build();
    }

}
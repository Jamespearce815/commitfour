package org.domco.connectfour.server.ui.presentation.service;

import org.domco.connectfour.server.ui.constants.Constant;
import org.domco.connectfour.server.ui.domain.PlayerColour;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the browser interaction service
 */
@Component
public class BrowserInteractionServiceHandler implements BrowserInteractionService {

    private WebDriver driver;

    private Wait<WebDriver> wait;

    /**
     * {@inheritDoc}
     */
    public void newBrowserTab() {
        ((JavascriptExecutor) driver).executeScript("window.open(arguments[0])", Constant.BASE_URI);
    }

    /**
     * {@inheritDoc}
     */
    public void switchTabFocus(PlayerColour playerColour) {

        List<String> winHandles = new ArrayList<>(driver.getWindowHandles());

//      Once yellow plays a winning piece then the page gets mapped to the yellow player object. If page is mapped
//      in < 0.5 seconds then it will be mapped before the grid was updated resulting  in an incorrect test failure.
        new WebDriverWait(driver, 1);

        if (playerColour.equals(PlayerColour.YELLOW)) {

            driver.switchTo().window(winHandles.get(0));

        } else if (playerColour.equals(PlayerColour.RED)) {

            driver.switchTo().window(winHandles.get(1));

        } else {
            throw new IllegalStateException("A PlayerColour of " + Constant.RED + " or " + Constant.YELLOW + " was not specified");
        }

    }

    @Override
    public Wait<WebDriver> getWait() {
        return wait;
    }

    @Override
    public WebDriver getDriver() {
        return driver;
    }

    @Override
    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    @Override
    public void setWait(Wait<WebDriver> wait) {
        this.wait = wait;
    }

}

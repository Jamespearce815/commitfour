package org.domco.connectfour.server.ui.service;

import org.domco.connectfour.server.ui.domain.Game;
import org.domco.connectfour.server.ui.domain.PlayerColour;

/**
 * Interface for the game play service.
 */
public interface GamePlayService {

    /**
     * A piece will be played in the designated column for the designated player
     *
     * @param playerColourToPlayPiece   the player colour that will be performing the move
     * @param columnNumber the column the player should drop the piece in
     */
    void dropPieceInColumn(PlayerColour playerColourToPlayPiece, int columnNumber);

    /**
     * Checks the game is over with the correct state for all players
     *
     * @param expectedGame the expected game after the test has run
     */
    void assertExpectedGameMatchesActualGame(Game expectedGame);
}

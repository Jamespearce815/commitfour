package org.domco.connectfour.server.ui.service;

import org.domco.connectfour.server.ui.domain.Game;
import org.domco.connectfour.server.ui.domain.GamePlayer;
import org.domco.connectfour.server.ui.domain.PlayerColour;
import org.domco.connectfour.server.ui.presentation.service.GamePlayPresentationService;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Implementation for Game Play service
 */
@Component
public class GamePlayServiceHandler implements GamePlayService {

    private static final Logger logger = LoggerFactory.getLogger(GamePlayServiceHandler.class);

    private TestStateContainer testStateContainer;
    private GamePlayPresentationService gamePlayPresentationService;

    public GamePlayServiceHandler(TestStateContainer testStateContainer, GamePlayPresentationService gamePlayPresentationService) {
        this.testStateContainer = testStateContainer;
        this.gamePlayPresentationService = gamePlayPresentationService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dropPieceInColumn(PlayerColour playerColourToPlayMove, int columnNumber) {

        logger.debug("Playing piece for the " + playerColourToPlayMove + " player");

        GamePlayer yellowGamePlayer = testStateContainer.getGame().getGamePlayerList().get(0);
        GamePlayer redGamePlayer = testStateContainer.getGame().getGamePlayerList().get(1);

        List<GamePlayer> updatedGamePlayers = gamePlayPresentationService
                .dropPieceInColumn(playerColourToPlayMove, yellowGamePlayer, redGamePlayer, columnNumber);

        testStateContainer.addUpdatedPlayerToGameObject(yellowGamePlayer, updatedGamePlayers.get(0));
        testStateContainer.addUpdatedPlayerToGameObject(redGamePlayer, updatedGamePlayers.get(1));

        logger.debug("Updated Game object with the updated players");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void assertExpectedGameMatchesActualGame(Game expectedGame) {

        logger.debug("Asserting the actual game matches the expected game");

        //1) Actual game object
        Game actualGame = testStateContainer.getGame();

        //2) Take playerID out of overall comparison (but verify format)
        // as there is no way to know the UUID before the test.
        checkPlayerIdIndividually(expectedGame, actualGame);

        //3) Assert
        Assert.assertEquals(expectedGame, actualGame);

    }

    /**
     * The PlayerId is a randomly generated UUID. Therefore we can't provide an expected value.
     * Each playerId is taken out of the assertion by setting the expected playerId to the actual player Id.
     * Each player Id is then verified to confirm it is of the correct format.
     *
     * @param expectedGame the expected game after the test has run
     * @param actualGame the actual game after the test has run
     */
    private void checkPlayerIdIndividually(Game expectedGame, Game actualGame) {

        for (int i =0; i < expectedGame.getGamePlayerList().size(); i++ ) {

            expectedGame.getGamePlayerList().get(i).getUiPageData().getGamePageData()
                    .setPlayerId(testStateContainer.getGame()
                            .getGamePlayerList()
                            .get(i)
                            .getUiPageData()
                            .getGamePageData()
                            .getPlayerId()
                    );

            //Now the playerId format needs to be checked as it was taken out of initial the comparison
            String uuidRegex = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}";
            Assert.assertTrue(actualGame.getGamePlayerList().get(i).getUiPageData()
                    .getGamePageData().getPlayerId().matches(uuidRegex));

        }

    }

}

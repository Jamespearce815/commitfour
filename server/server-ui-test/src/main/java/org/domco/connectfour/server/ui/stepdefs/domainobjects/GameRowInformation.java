package org.domco.connectfour.server.ui.stepdefs.domainobjects;

import java.util.Arrays;
import java.util.List;

/**
 * Game Row information. A list of Game Row Information objects provides the game grid.
 */
public class GameRowInformation {

    //These fields must exist for the mapping from the cucumber table to be successful.
    private final String c0;
    private final String c1;
    private final String c2;
    private final String c3;
    private final String c4;
    private final String c5;
    private final String c6;

    //Constructor isn't used in the mapping from cucumber table to GameRowInformation fields
    // but allows them to be immutable
    public GameRowInformation(String c0, String c1, String c2, String c3, String c4, String c5, String c6) {
        this.c0 = c0;
        this.c1 = c1;
        this.c2 = c2;
        this.c3 = c3;
        this.c4 = c4;
        this.c5 = c5;
        this.c6 = c6;
    }

    /**
     * @return provides the pieces in this row as a list.
     */
    public List<String> fetchColumnPiecesForRow() {
        return Arrays.asList(c0, c1, c2, c3, c4, c5, c6);
    }

}

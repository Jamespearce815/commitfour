package org.domco.connectfour.server.ui.stepdefs.domainobjects;

/**
 * Game State Information.
 */
public class GameStateInformation {

    private final String playerName;
    private final String playerColour;
    private final String opponentName;
    private final String gameStatus;
    private final Integer gameHeight;
    private final Integer gameWidth;
    private final String colourJustPlayed;

    public GameStateInformation(String playerName, String playerColour, String opponentName, String gameStatus,
                                int gameHeight, int gameWidth, String colourJustPlayed) {
        this.playerName = playerName;
        this.playerColour = playerColour;
        this.opponentName = opponentName;
        this.gameStatus = gameStatus;
        this.gameHeight = gameHeight;
        this.gameWidth = gameWidth;
        this.colourJustPlayed = colourJustPlayed;
    }

    public String getPlayerName() {
        return playerName;
    }

    public String getPlayerColour() {
        return playerColour;
    }

    public String getOpponentName() {
        return opponentName;
    }

    public String getGameStatus() {
        return gameStatus;
    }

    public int getGameHeight() {
        return gameHeight;
    }

    public int getGameWidth() {
        return gameWidth;
    }

    public String getColourJustPlayed() {
        return colourJustPlayed;
    }
}

package org.domco.connectfour.server.ui.stepdefs.mappers;

import org.domco.connectfour.server.ui.domain.Game;
import org.domco.connectfour.server.ui.stepdefs.domainobjects.GameRowInformation;
import org.domco.connectfour.server.ui.stepdefs.domainobjects.GameStateInformation;

import java.util.List;

/**
 * Game Information Mapper Interface.
 */
public interface GameInformationMapper {

    /**
     * No gameRowInformationList is passed in as the game has not begun.
     * @param gameStateInformationList state of the game including player information, height, width and who just played a piece
     * @return expected game object for assertion at the end of a test
     */
    Game mapToExpectedGame(List<GameStateInformation> gameStateInformationList);


    /**
     * @param gameStateInformationList state of the game including player information, height, width and who just played a piece
     * @param gameRowInformationList   the pieces played on the game grid
     * @return expected game object for assertion at the end of a test
     */
    Game mapToExpectedGame(List<GameStateInformation> gameStateInformationList, List<GameRowInformation> gameRowInformationList);

}
